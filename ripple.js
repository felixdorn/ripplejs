
/**
 *
 * @param {string} element The targeted element
 * @param {string} color The color of the ripple
 * @param {string} opacity Opacity of the ripple
 * @param {string} stop Where the ripple stop
 */
function ripple(element, color ='255,255,255', opacity = 0.3, stop = 120) {
  var rgb;
  var els = document.querySelectorAll(element);
  els.forEach(el => {
    el.onclick = function(evt) {
      var x = evt.pageX - el.offsetLeft;
      var y = evt.pageY - el.offsetTop;

      var duration = 1000;
      var animationFrame, animationStart;

      var animationStep = function(timestamp) {
        if (!animationStart) {
          animationStart = timestamp;
        }

        var frame = timestamp - animationStart;
        if (frame < duration) {
          var easing = (frame / duration) * (2 - frame / duration);

          var circle = "circle at " + x + "px " + y + "px";
          var clr = "rgba(" + color + "," + opacity * (1 - easing) + ")";
          var stp = stop * easing + "%";
          el.style.backgroundImage =
            "radial-gradient(" +
            circle +
            ", " +
            clr +
            " " +
            stp +
            ", transparent " +
            stp +
            ")";

          animationFrame = window.requestAnimationFrame(animationStep);
        } else {
          el.style.backgroundImage = "none";
          window.cancelAnimationFrame(animationFrame);
        }
      };

      animationFrame = window.requestAnimationFrame(animationStep);
    };
  });
}
