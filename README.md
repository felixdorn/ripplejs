# How to use ripple.js ?
> Ripple effect according to the [material design guidelines](https://material.io/design/)

> 1.5kb library

> No dependencies
> Easy to use.
## Params
* `element` : Targeted element
* `color` : Color of the ripple effect *optional*
* `opacity` : Opacity of the ripple effect *optional*
* `stop` : Where the ripple stop *optional*

## Ripple function (e.g)
```html
<input class="ripple" type="submit" value="hey" />

<div class="ripple">My Div</div>

<script src="ripple.js"></script>

<script>
    ripple(".ripple", "142, 68, 173");
<script>
```